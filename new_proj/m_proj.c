#include <stdio.h>
#include <string.h>
#include <dos.h>
#include "c_buffer.c"

/* UART */
#define THR 0x3F8 /* Registo de Dados de Transmissao */
#define RBR 0x3F8 /* Registo de Dados de Recepcao */
#define IER 0x3F9 /* Registo de Controlo das Interrupcoes */
#define MCR 0x3FC /* Registo de Controlo do Modem */

/* PIC */
#define LSR 0x3FD /* Registo de Estado */

#define IMR 0x21 /* Registo de Mascara */
#define ISR 0x20 /* Registo de Comandos */
#define EOI 0x20 /* Fim de Interrupcao */

/* Caracteres */
#define NL '\n'
#define EOL '\0'
#define MAX_CHAR 200
#define EXT_CHAR 'q'

/* Interrupcoes */
void UART_int_on();
void UART_int_off();

/* Rotina de Tratamento */
void interrupt (*old_isr)(); /* Rotina anterior */
void interrupt isr_rotina();
unsigned char recv_serial();

/* Enviar texto */
void sendSerial(unsigned char byte);
void sendLine(char *ptr);

/* Variaveis */


int main(int argc, char const *argv[]){
	char *ptr;
	char message[MAX_CHAR];
	UART_int_on();
	while((ptr = fgets(cadeia, MAX_CHAR, stdin)) != NULL) {
		if(cadeia[0] == EXT_CHAR && cadeia[1] == NL){
			break;
		} else {
			sendLine(ptr);

		}
	}
	UART_int_off();
	return 0;
}

/* Envia char a char */
void sendSerial(unsigned char byte){
	unsigned char s;
	do{
		s = inport(LSR);
	} while((s & EOI) == 0);
	outportb(THR, byte);
}

/* Le a cadeia e envia pelo sendSerial */
void sendLine(char *ptr){
	while(*ptr != EOL){
		sendSerial(*ptr);
		ptr++;
	}
}

void UART_int_on(){
/* Vai buscar o valor antigo da pos 12 do vetor de interrupcoes*/
	old_isr = getvect(12);
	setvect(12, isr);
	outportb(MCR, (inportb(MCR) | 0x8));
	outportb(IER, (inportb(IER) | 0x1));
	disable();
	outportb(IMR, (inportb(IMR) & 0xEF));
	enable();
}

void UART_int_off(){
	outportb(MCR, (inportb(MCR) & ~0x8));
	outportb(IER, (inportb(IER) & ~0x1));
	disable();
	outportb(IMR, inportb(IMR) | 0x10);
	enable();
	setvect(12, old_isr);
}

void interrupt isr_rotina(){
	char ch = inportb(RBR);
	if(bufFull()){

	} else {
		bufPut(ch);
	}
	outportb(ISR, EOI);
}

unsigned char recv_serial(){
	char ch;
	while(bufEmpty()){
		/* Esperar */
	}
	ch = bufGet();
	return ch;
}