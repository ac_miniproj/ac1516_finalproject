#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "buffer.h"


// Tamanho do buffer
#define BUF_SIZE 20



// Definicao do tipo "buffer_t"
// ATENCAO: 'buffer_t' e um apontador para uma estrutura do tipo 'buffer_s'
// typedef struct buffer_s *buffer_t;
// So precisa definir a estrutura "buffer_s" no ficheiro "BUFFER.C"
struct buffer_s {
    unsigned char bytes[BUF_SIZE];
    int put;
};


// Retorna um buffer devidamente inicializado
buffer_t initBuffer(void) {
    // Reserva espaco para o buffer, termina o programa se nao conseguir
    buffer_t buffer = malloc(sizeof(*buffer));
    if (buffer == NULL) {
        fprintf(stderr,"Error allocating buffer\n");
        exit(1);
    }
    buffer->put = 0;  // buffer vazio
    // Agora 'buffer' e' um apontador valido para um buffer_s
    return buffer;
}


// Adiciona o caracter 'c' ao buffer
int appendBuffer(buffer_t buffer, char c) {
	int result = 0;
	buffer->bytes[buffer->put++] = c;
	if(buffer->put == BUF_SIZE){
		result = 1;
	} else {
		result = 0;
	}
	return result;
}



// Esvazia o buffer
// Deve simplesmente escrever no terminal os caracteres contidos no buffer
void flushBuffer(buffer_t buffer) {
	write(1, buffer->bytes, buffer->put);
	buffer->put = 0;
	char c = '$';
	write(1, &c, 1);
}

