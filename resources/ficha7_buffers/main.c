#include <stdio.h>
#include <stdlib.h>
#include "buffer.h"
#include "myputchar.h"


int main (int argc, char *argv[]) {
    FILE *f;
    int c;
    buffer_t buffer;

    // Verificar os argumentos da linha de comando
    if (argc != 2) {
        fprintf(stderr,"Usage: %s <filename>\n", argv[0]);
        exit(1);
    }

    // Tentar abrir o ficheiro para leitura
    f=fopen (argv[1], "r");
    if (f== NULL) {
        fprintf(stderr,"Invalid file: %s\n", argv[1]);
        exit(1);
    }
    
    // Inicializa o buffer
    buffer = initBuffer();

    // Ler os caracteres do ficheiro e guarda-los no buffer
    while ( (c=fgetc(f))!=EOF ) {
        myPutChar(buffer, c);
    }

    // Fechar o ficheiro
    fclose(f);

    // Imprimir o resto do buffer
    flushBuffer(buffer);
    
    // Terminar
    return 0;
}

