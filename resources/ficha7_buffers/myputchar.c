#include <stdio.h>
#include "myputchar.h"


// Adiciona um carater ao buffer
// Se (e so se) o caracter inserido for uma mudanca
//        de linha ou o buffer ficou cheio
//        invoca a funcao 'flushBuffer'
void myPutChar(buffer_t buffer, char c) {
	int opResult = appendBuffer(buffer, c);
	if(c == '\n' || opResult){
		flushBuffer(buffer);
	}
}
