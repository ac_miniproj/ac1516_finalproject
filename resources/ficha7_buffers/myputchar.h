#ifndef __MYPUTCHAR_H
#define __MYPUTCHAR_H

#include "buffer.h"

// Funcao a implementar no ficheiro "MYPUTCHAR.C"
/// Se (e so se) o caracter inserido for uma mudanca
//        de linha ou o buffer ficou cheio
//        invoca a funcao 'flushBuffer'
extern void myPutChar(buffer_t buffer, char c);

#endif
