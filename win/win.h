#ifdef _WIN_H
#define _WIN_H
/****
 *  WIN.H
 *  vad - Maio 2016 - AC/MIEI - FCT/UNL
 *  usa conio para manipular o ecra diretamente
 *  e simular duas janelas como num chat
 ****/

void win_init(void);

void win_delbox(void);

void win_puts(char*s);


#endif
