#define MAX_CHAR 80

unsigned char buffer[MAX_CHAR];
int put = 0;
int get = 0;
int nc = 0;

void bufPut(unsigned char c){
	buffer[put] = c;
	put = (put + 1) % MAX_CHAR;
	nc++;
}

unsigned char bufGet(){
	unsigned char ch = buffer[get];
	get = (get + 1) % MAX_CHAR;
	nc--;
	return ch;
}

int bufFull(){
	return nc == MAX_CHAR;
}

int bufEmpty(){
	return nc == 0;
}

	
