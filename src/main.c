#include <stdio.h>
#include <string.h>
#include <dos.h>

#define THR 0x3F8
#define RBR 0x3F8
#define IER 0x3F9
#define MCR 0x3FC
#define LSR 0xFD
#define IMR 0x20
#define ISR 0x21
#define EOI 0x20
#define NL '\n'
#define EOL '\0'
#define MAX_CHAR 80
#define EXT_CHAR 'q'

extern unsigned char inportb(int port);
extern void outportb(int port, unsigned char byte);
extern void bufPut(unsigned char c);
extern unsigned char bufGet();
extern int bufFull();
extern int bufEmpty();

void interrupt (*old_isr)();

/* Envia char a char */
void sendSerial(unsigned char byte){
	unsigned char s;
	do{
		s = inport(LSR);
	} while((s & EOI) == 0);
	outportb(THR, byte);
}

/* Le a cadeia e envia pelo sendSerial */
void sendLine(char *ptr){
	while(*ptr != EOL){
		sendSerial(*ptr);
		ptr++;
	}
}

unsigned char receiveSerial(){
	unsigned char ch;
	while(bufEmpty()){
		/* esperar */
	}
	ch = bufGet();
	return ch;
}

void interrupt RX_exception(){
	unsigned char ch = inport(RBR);
	if(bufFull()){
		/* ERRO */
	} else {
		bufPut(ch);
	}
	outportb(ISR, EOI);
}

void UART_int_on(){
	/* Vai buscar o valor antigo da posição 12 do vetor de interrupcoes*/
	old_isr = getvect(12);
	setvect(12, RX_exception);
	outportb(MCR, (inportb(MCR) | 0x8));
	outportb(IER, (inportb(IER) | 0x1));
	disable();
	outportb(RM, (inportb(RM) & 0xEF));
	enable(); 
}

void UART_int_off(){
	outportb(MCR, (inportb(MCR) & ~0x8));
	outportb(IER, (inportb(IER) & ~0x1));
	disable();
	outportb(RM, (inportb(RM) | ~0xEF);
	enable(); 
	setvect(12, old_isr);
}

int main(int argc, char const *argv[]){
	char *ptr;
	char cadeia[MAX_CHAR];
	
	do{
		ptr = fgets(cadeia, MAX_CHAR, stdin);
		if(cadeia[0] == EXT_CHAR && cadeia[1] == NL){
			break;
		} else {
			sendLine(ptr);
		}
		write(1, receiveSerial(), MAX_CHAR);
	} while (strlen(ptr) != 2 || cadeia[0] != EXT_CHAR);
	
	return 0;

}
